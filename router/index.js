import VueRouter from 'vue-router';
import Products from '@/components/Products'
import Product from '@/components/Product'
import Category from '@/components/Category'
import Checkout from '@/components/Checkout'
import Admin from '@/components/Admin'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
const Profile = () => import('@/components/Profile.vue')
const BoardAdmin = () => import("@/components/BoardAdmin.vue")
const BoardModerator = () => import("@/components/BoardModerator.vue")
const BoardUser = () => import("@/components/BoardUser.vue")
import store from '../store/index'
import Vue from 'vue'
import Vuex from "vuex"


Vue.use(Vuex);
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name:'home',
        component: Products
    },
    {
        path: "/profile",
        name: "profile",
        // lazy-loaded
        component: Profile,
      },
      {
        path: "/admin",
        name: "admin",
        // lazy-loaded
        component: BoardAdmin,
      },
      {
        path: "/mod",
        name: "moderator",
        // lazy-loaded
        component: BoardModerator,
      },
      {
        path: "/user",
        name: "user",
        // lazy-loaded
        component: BoardUser,
      },
    {
        path: '/product/:id',
        name: 'product',
        component: Product,
        props: true
    },
    {
        path: '/category/:category',
        name: 'category',
        component: Category,
        props: (route) => ({ category: route.params.category, filters: route.query })
    },
    {
        path: '/checkout',
        name: 'checkout',
        component: Checkout,
    },

    {
        path: "/login",
        name: "login",
        component: Login
    },

    {
        path: "/register",
        component: Register
    },
    {
        path: "/admin",
        name: "admin",
        component: Admin,
        beforeEnter: (to, from, next) => {
            if(store.getters.getAuthentication == false) {
                next(false);
            } else {
                next();
            }
        }
    },

];

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router;
